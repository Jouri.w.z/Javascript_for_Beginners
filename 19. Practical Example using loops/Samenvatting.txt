What:
Practical example of using loops
how loops in javascript can cicle through your webpage and in HTML

How:
getElementsByTagName // gets all the elements by name

var links = document.getElementsByTagName("a"); // creates new variable

for (i = 0; i < links.length; 1++){			// i starts as 0, if i (0) is less then the length of links add 1
	links[i].className = "links-" + i;		// gives the links a class name with the right number.
}


Results:
var links = document.getElementsByTagName("a");

for (i = 0; i < links.length; 1++){
	links[i].className = "links-" + i;
}
