what:
break; 			// breaks you out of the loop for good.
continue; 		// skips out the code for the iteration below it.
return false; 	// is het stoppen van een functie

How:
for (i = 0; i < 10; i++){ 	// for loop

	if (i ===5 || i ===3){ 	// if i is equal to 5 and 3
	continue;				// skip 5 and 3

	}
	console.log(i);			// logs i in console

	if(i === 7){			// if 7 === 7 do..
		break;				// break loop
	}


Result:
for (i = 0; i < 10; i++){

	if (i ===5 || i ===3){
	continue;

	}
	console.log(i);

	if(i === 7){
		break;
	}
}

console.log("I have broken out of the loop");
